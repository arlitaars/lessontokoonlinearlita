package com.example.arlita.lessonlistview;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemAdapter extends CursorAdapter {

    DBHelper mydb;

    public class ViewHolder{
        ImageView imageView;
        TextView itemName;
        TextView itemPrice;
        Button btnPilih;
        Button btnView;
    }

    public ItemAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.single_item, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        // Find fields to populate in inflated template
        ViewHolder holder = new ViewHolder();
        mydb = new DBHelper(this.mContext);
        final SQLiteDatabase db = mydb.getWritableDatabase();

        if(view == null)
        {
            holder.itemName = view.findViewById(R.id.itemName);
            holder.itemPrice = view.findViewById(R.id.itemPrice);
            holder.btnView = view.findViewById(R.id.btnView);
            holder.btnPilih = view.findViewById(R.id.btnPilih);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
            if(cursor.getInt(cursor.getColumnIndexOrThrow("status")) == 1) {
                holder.btnPilih.setText("BATAL");
            } else {
                holder.btnPilih.setText("PILIH");
            }
        }

        final Button btnPilih = view.findViewById(R.id.btnPilih);
        Button btnView = view.findViewById(R.id.btnView);

        // Extract properties from cursor
        final String name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        final String price = cursor.getString(cursor.getColumnIndexOrThrow("price"));
        // Populate fields with extracted properties
        TextView itemName = view.findViewById(R.id.itemName);
        TextView itemPrice = view.findViewById(R.id.itemPrice);

        itemName.setText(name);
        itemPrice.setText(price);

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), SingleViewAcitivity.class);
                i.putExtra("parse_nama", name);
                i.putExtra("parse_harga", price);
            }
        });

        btnPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cursor.getInt(cursor.getColumnIndexOrThrow("status")) == 1){
                    btnPilih.setText("BATAL");

                    Integer id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
                    ContentValues values = new ContentValues();
                    values.put(DBHelper.PRODUCT_COLUMN_STATUS, 0);
                    db.update("produks", values, "id = ? ", new String[] { Integer.toString(id) } );
                }else{
                    btnPilih.setText("BATAL");
                    Integer id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
                    ContentValues values = new ContentValues();
                    values.put(DBHelper.PRODUCT_COLUMN_STATUS, 1);
                    db.update("produks", values, "id = ? ", new String[] { Integer.toString(id) } );
                }
            }
        });
    }
}
