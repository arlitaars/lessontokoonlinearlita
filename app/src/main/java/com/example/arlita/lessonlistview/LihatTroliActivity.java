package com.example.arlita.lessonlistview;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

public class LihatTroliActivity extends AppCompatActivity {

    // TodoDatabaseHandler is a SQLiteOpenHelper class connecting to SQLite
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_barang);

        DBHelper handler = new DBHelper(this);

        // Get access to the underlying writeable database
        SQLiteDatabase db = handler.getWritableDatabase();
        // Query for items from the database and get a cursor back

        Cursor todoCursor = db.rawQuery("SELECT  * FROM produks WHERE produks.status = 1", null);

        // Find ListView to populate
        ListView lvItem = findViewById(R.id.lvItem);
        // Setup cursor adapter using cursor from last step
        ItemAdapter itemAdapter = new ItemAdapter(this, todoCursor);
// Attach cursor adapter to the ListView
        lvItem.setAdapter(itemAdapter);
    }
}