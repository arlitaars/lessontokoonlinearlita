package com.example.arlita.lessonlistview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    Button btnLihatBrg;
    Button btnLihatTrl;
    DBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Construct the data source
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mydb = new DBHelper(this);
        this.initItems();

        btnLihatBrg = findViewById(R.id.btnLihatBrg);
        btnLihatTrl = findViewById(R.id.btnLihatTrl);

        btnLihatBrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), LihatBarangActivity.class);
                v.getContext().startActivity(i);
            }
        });

        btnLihatTrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), LihatTroliActivity.class);
                v.getContext().startActivity(i);
            }
        });

    }

    public void initItems() {
        mydb.insertContact("Baju BTS", "RP 175.000", R.drawable.baju1 + "", 0);
        mydb.insertContact("Boneka Bangtan", "RP 180.000", R.drawable.boneka_bangtan + "", 0);
        mydb.insertContact("Jaket GD", "RP 215.000", R.drawable.gd_jaket + "", 0);
        mydb.insertContact("Lightstick Wanna One", "RP 310.000", R.drawable.w1_ls + "", 0);
        mydb.insertContact("Lishtstick EXO", "RP 310.000", R.drawable.exo_ls + "", 0);
        mydb.insertContact("Lightstick Monsta X", "RP 310.000", R.drawable.mx_ls + "", 0);
        mydb.insertContact("Tas EXO", "RP 278.000", R.drawable.tas_bts + "", 0);
        mydb.insertContact("Gelang BTS", "RP 190.000", R.drawable.gelang1 + "", 0);
        mydb.insertContact("Key Ring iKon", "RP 180.000", R.drawable.ikon_keyring + "", 0);
    }
}
