package com.example.arlita.lessonlistview;

public class Item {
    public Integer image;
    public String name;
    public String price;

    public Item(Integer image, String name, String price) {
        this.image = image;
        this.name = name;
        this.price = price;
    }
}
